
/**
 * Write a description of Part1 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import edu.duke.StorageResource;

public class Part1 {
    public int findStopCodon(String dna, int startIndex, String stopCodon) {
        int currIndex = dna.indexOf(stopCodon, startIndex+3);
        if ((currIndex - startIndex) % 3 == 0) {
            return currIndex;
        }
        return dna.length();
    }
    
    public void testFindStopCodon() {
        System.out.println(findStopCodon("ATGTAA", 0, "TAA"));
        System.out.println(findStopCodon("ATGATAA", 0, "TAA"));
    }
    
    public String findGene(String dna, int position) {
        int currIndex = 0;
        int startIndex = dna.indexOf("ATG", position);
        if (startIndex == -1) {
            return "";
        }
        int taaIndex = findStopCodon(dna, startIndex, "TAA");
        int tgaIndex = findStopCodon(dna, startIndex, "TGA");
        int tagIndex = findStopCodon(dna, startIndex, "TAG");
        currIndex = Math.min(taaIndex, (Math.min(tgaIndex, tagIndex)));
        if (currIndex == dna.length()) {
            return "";
        }
        return dna.substring(startIndex, currIndex+3);
    }
    
    public void testFindGene() {
        System.out.println(findGene("ATGTAATAG", 0));
        System.out.println(findGene("TAATAG", 0));
        System.out.println(findGene("ATGAAAAAAAAAAAAAAAAAAAAAAAAA", 0));
        System.out.println(findGene("ATGAAAAAAAAAAAATAA", 0));
    }
    
    public void printAllGenes(String dna) {
        int position = 0;
        while (true) {
            String currentGene = findGene(dna, position);
            if (currentGene.isEmpty()) {
                break;
            }
            System.out.println(currentGene);
            position = dna.indexOf(currentGene, position) + currentGene.length();
        }
    }
    
    public void testPrintAllGenes() {
        printAllGenes("ATGATCTAATTTATGCTGCAACGGTGAAGA");
    }
    
    public StorageResource getAllGenes(String dna) {
        StorageResource geneList = new StorageResource();
        int position = 0;
        while (true) {
            String currentGene = findGene(dna, position);
            if (currentGene.isEmpty()) {
                break;
            }
            geneList.add(currentGene);
            position = dna.indexOf(currentGene, position) + currentGene.length();
        }
        return geneList;
    }
    
    public void testGetAllGenes() {
        StorageResource geneList = getAllGenes("ATGATCTAATTTATGCTGCAACGGTGAAGA");
        for (String s : geneList.data()) {
            System.out.println(s);
        }
    }
}

