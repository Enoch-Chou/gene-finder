
/**
 * Write a description of Part2 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Part2 {
    public float cgRatio(String dna) {
        int cCounter = 0;
        int gCounter = 0;
        for (int i = 0; i < dna.length(); i++) {
            if (dna.substring(i,i+1).equals("C")) {
                cCounter++;
            }
            if (dna.substring(i,i+1).equals("G")) {
                gCounter++;
            }
        }
        return (float) cCounter / gCounter;
    }
    
    public int countCTG(String dna) {
        int CTGCounter = 0;
        for (int i = 0; i < dna.length()-3; i++) {
            if (dna.substring(i, i+3).equals("CTG")) {
                CTGCounter++;
            }
        }
        return CTGCounter;
    }
}
